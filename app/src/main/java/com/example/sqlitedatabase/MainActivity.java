package com.example.sqlitedatabase;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mName = (EditText) findViewById(R.id.etName);
        mNumber = (EditText) findViewById(R.id.etNumber);
        mDbHelper = new DBHelper(this);
    }

    public void onClick(View view) {
        Context context = view.getContext();
        switch (view.getId()) {
            case R.id.btnSave:
        }
        mDbHelper.addNameNumber(mName.getText().toString(), mNumber.getText().toString());
        break;
        case R.id.btnFind:
        String number = mDbHelper.findNumberFromName(mName.getText().toString());
        if (number != null) {
            Toast.makeText(context, number, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "There is no listening for that name",
                    Toast.LENGTH_LONG).show();
        }
        break;
    }
}